
import 'package:flutter/material.dart';

class ScaffoldExample extends StatelessWidget{
  const ScaffoldExample({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Scaffold"),
        centerTitle: true,
        backgroundColor: Colors.amberAccent.shade700,
      ),
        backgroundColor: Colors.redAccent.shade100,
        body:Container(
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:<Widget>[
             IconButton(icon: const Icon(Icons.access_alarm_rounded),onPressed:_tapButton)
            ]
          )
        ) ,
        floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightBlueAccent,
          onPressed: () => debugPrint("Bye bye") ,
        ),
      bottomNavigationBar: BottomNavigationBar(
      items: const [
        BottomNavigationBarItem(icon: Icon(Icons.email), label: "Email"),
        BottomNavigationBarItem(icon: Icon(Icons.account_circle), label:"Deux"),
        BottomNavigationBarItem(icon: Icon(Icons.add_to_home_screen), label:"Add Homescreen"),
      ],
        onTap: (int index) => debugPrint("Item cliqué : $index"),
    ),
    ) ;
  }



  void _tapButton() {
    debugPrint("Tapped Button !");
  }
}